package com.demo.mircoservice1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mircoservice1Application {

	public static void main(String[] args) {
		SpringApplication.run(Mircoservice1Application.class, args);
	}

}
